#!/usr/bin/env python
# -*- coding: utf-8 -*-

import abc
import json
import datetime
import logging
import hashlib
import numbers
import uuid
from optparse import OptionParser
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler

import scoring
from my_validation.entity import Entity
from my_validation.fields import (
    ArgumentsField, BirthdayField, CharField, ClientIDsField,
    DateField, EmailField, GenderField, PhoneField
)

SALT = "Otus"
ADMIN_LOGIN = "admin"
ADMIN_SALT = "42"
OK = 200
BAD_REQUEST = 400
FORBIDDEN = 403
NOT_FOUND = 404
INVALID_REQUEST = 422
INTERNAL_ERROR = 500
ERRORS = {
    BAD_REQUEST: "Bad Request",
    FORBIDDEN: "Forbidden",
    NOT_FOUND: "Not Found",
    INVALID_REQUEST: "Invalid Request",
    INTERNAL_ERROR: "Internal Server Error",
}


def not_empty(value):
    """
    вынес из всех классов, как-то нигде оно как метод не уместно
    наверное можно бы было завести модуль с хелперами

    :param value:
    :return:
    """
    return bool(value) or isinstance(value, numbers.Integral)


def all_not_empty(*args):
    """
    еще один хелпер
    проверяет, что все аргументы not_empty

    :param args:
    :return:
    """
    return all(not_empty(x) for x in args)


class ClientsInterestsRequest(Entity):
    client_ids = ClientIDsField(required=True, nullable=False)
    date = DateField(required=False, nullable=True)


class OnlineScoreRequest(Entity):
    first_name = CharField(required=False, nullable=True)
    last_name = CharField(required=False, nullable=True)
    email = EmailField(required=False, nullable=True)
    phone = PhoneField(required=False, nullable=True)
    birthday = BirthdayField(required=False, nullable=True)
    gender = GenderField(required=False, nullable=True)

    def _validate(self):
        super(OnlineScoreRequest, self)._validate()

        additional_validation = [
            ("phone", "email"), ("first_name", "last_name"), ("gender", "birthday")
        ]
        # True если хотя бы для одной из пар оба ключа существуют и мапятся на непустые значения
        valid = any(all_not_empty(self._data.get(x, None), self._data.get(y, None)) for x, y in additional_validation)
        if not valid:
            self._errors.append('request must contain at least one of the pairs '
                                '("phone", "email"), ("first_name", "last_name"), '
                                '("gender", "birthday") non empty')


class MethodRequest(Entity):
    account = CharField(required=False, nullable=True)
    login = CharField(required=True, nullable=True)
    token = CharField(required=True, nullable=True)
    arguments = ArgumentsField(required=True, nullable=True)
    method = CharField(required=True, nullable=False)

    @property
    def is_admin(self):
        return self.login == ADMIN_LOGIN


class MethodHandler(object):
    def __init__(self, store, request, ctx):
        self.store = store
        self.request = request
        self.ctx = ctx

    @abc.abstractmethod
    def handle(self):
        pass


class OnlineScoreHandler(MethodHandler):
    def handle(self):
        try:
            if self.request.is_admin:
                response, code = {'score': 42}, OK
            else:
                arguments = OnlineScoreRequest(self.request.arguments)
                if arguments.errors:
                    raise ValueError(arguments.errors)

                self.ctx['has'] = arguments.has

                score = scoring.get_score(
                    self.store,
                    arguments.phone,
                    arguments.email,
                    arguments.first_name,
                    arguments.last_name,
                    arguments.birthday,
                    arguments.gender
                )
                response, code = {'score': score}, OK
        except ValueError as e:
            response, code = {"code": INVALID_REQUEST, "error": '\n'.join(e.args[0])}, INVALID_REQUEST
        finally:
            return response, code


class ClientsInterestsHandler(MethodHandler):
    def handle(self):
        arguments = ClientsInterestsRequest(self.request.arguments)
        if arguments.errors:
            raise ValueError(arguments.errors)
        self.ctx['nclients'] = len(arguments.client_ids)
        response = {id_: scoring.get_interests(self.store, id_) for id_ in arguments.client_ids}
        return response, OK


def check_auth(request):
    if request.is_admin:
        digest = hashlib.sha512(datetime.datetime.now().strftime("%Y%m%d%H") + ADMIN_SALT).hexdigest()
    else:
        digest = hashlib.sha512(request.account + request.login + SALT).hexdigest()
    if digest == request.token:
        return True
    return False


def method_handler(raw_request, ctx, store):
    handlers = {
        'online_score': OnlineScoreHandler,
        'clients_interests': ClientsInterestsHandler,
    }

    body, headers = raw_request.get('body', {}), raw_request.get('headers', {})
    try:
        request = MethodRequest(body)
        if request.errors:
            raise ValueError(request.errors)
        if not check_auth(request):
            response, code = {"code": FORBIDDEN, "error": ERRORS[FORBIDDEN]}, FORBIDDEN
        else:
            method = request.method
            handler_cls = handlers[method]
            handler = handler_cls(store, request, ctx)
            response, code = handler.handle()
    except ValueError as e:
        response, code = {'code': INVALID_REQUEST, 'error': '\n'.join(e.args[0])}, INVALID_REQUEST
    except Exception:
        response, code = {'code': INTERNAL_ERROR, 'error': ERRORS[INTERNAL_ERROR]}
    finally:
        return response, code


class MainHTTPHandler(BaseHTTPRequestHandler):
    router = {
        "method": method_handler
    }
    store = None

    def get_request_id(self, headers):
        return headers.get('HTTP_X_REQUEST_ID', uuid.uuid4().hex)

    def do_POST(self):
        response, code = {}, OK
        context = {"request_id": self.get_request_id(self.headers)}
        request = None
        try:
            data_string = self.rfile.read(int(self.headers['Content-Length']))
            request = json.loads(data_string)
        except:
            code = BAD_REQUEST

        if request:
            path = self.path.strip("/")
            logging.info("%s: %s %s" % (self.path, data_string, context["request_id"]))
            if path in self.router:
                try:
                    response, code = self.router[path]({"body": request, "headers": self.headers}, context, self.store)
                except Exception as e:
                    logging.exception("Unexpected error: %s" % e)
                    code = INTERNAL_ERROR
            else:
                code = NOT_FOUND

        self.send_response(code)
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        if code not in ERRORS:
            r = {"response": response, "code": code}
        else:
            r = {"error": response or ERRORS.get(code, "Unknown Error"), "code": code}
        context.update(r)
        logging.info(context)
        self.wfile.write(json.dumps(r))
        return


if __name__ == "__main__":
    op = OptionParser()
    op.add_option("-p", "--port", action="store", type=int, default=8080)
    op.add_option("-l", "--log", action="store", default=None)
    (opts, args) = op.parse_args()
    logging.basicConfig(filename=opts.log, level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
    server = HTTPServer(("localhost", opts.port), MainHTTPHandler)
    logging.info("Starting server at %s" % opts.port)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
